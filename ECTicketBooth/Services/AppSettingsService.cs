﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECTicketBooth.DataAccess;

namespace ECTicketBooth.Services
{
    class AppSettingsService
    {
        public AppSettingsService()
        {

        }

        public int GetValue(string settingName)
        {
            return DAO.Select<Setting>(
                new string[] { "name" }, new object[] { settingName })[0].Value;
        }

        public void SetValue(string settingName, int settingValue)
        {
            DAO.Update(new Setting(settingName, settingValue));
        }

    }
}
