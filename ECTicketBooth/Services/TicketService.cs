﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECTicketBooth.DataAccess;
using System.IO;
using System.Diagnostics;

namespace ECTicketBooth.Services
{
    public enum TicketType
    {
        AllDays = 1,
        Day1 = 2,
        Day2 = 3,
        Day3 = 4
    }

    class TicketService
    {
        public int MaxSold
        {
            get
            {
                int allDayTickets = DAO.Count<Ticket>(
                    new string[] { "type" }, new object[] { (int)TicketType.AllDays });
                int day1Tickets = DAO.Count<Ticket>(
                    new string[] { "type" }, new object[] { (int)TicketType.Day1 });
                int day2Tickets = DAO.Count<Ticket>(
                    new string[] { "type" }, new object[] { (int)TicketType.Day2 });
                int day3Tickets = DAO.Count<Ticket>(
                    new string[] { "type" }, new object[] { (int)TicketType.Day3 });

                return Math.Max(Math.Max(Math.Max(allDayTickets + day1Tickets, allDayTickets + day2Tickets), allDayTickets + day3Tickets), allDayTickets);
            }
        }

        public TicketService()
        {
            
        }

        public List<string> GetTicketHistory(int seller)
        {
            var tickets = DAO.Select<Ticket>(
                new string[] { "seller" },
                new object[] { seller });
            var history = tickets.Select(t => ((TicketType)t.Type).ToString() + " - " + t.SoldTime).ToList();
            history.Reverse();
            return history;
        }
        
        public int GetProfitByCashier(string seller)
        {
            int userId = DAO.Select<Cashier>(new string[] { "name" }, new object[] { seller })[0].Id;
            return GetProfitByCashier(userId);
        }

        public int GetTicketsSoldCount(int seller)
        {
            return DAO.Count<Ticket>(new string[] { "seller" }, new object[] { seller });
        }

        public int GetProfitByCashier(int seller)
        {
            int allDays = DAO.Count<Ticket>(
                new string[] { "seller", "type" },
                new object[] { seller, (int)TicketType.AllDays });
            int individualDays = DAO.Count<Ticket>(
                new string[] { "seller", "type" },
                new object[] { seller, new object[] { (int)TicketType.Day1, (int)TicketType.Day2, (int)TicketType.Day3 } });

            return allDays * 400 + individualDays * 200;
        }

        public int GetProfitByDay(DateTime dateTime)
        {
            var interval = new DataInterval<string>(
                dateTime.Date.ToString("yyyy-MM-dd"),
                dateTime.Date.AddDays(1).ToString("yyyy-MM-dd"),
                true, false);

            int allDays = DAO.Count<Ticket>(
                new string[] { "soldTime", "type" },
                new object[] { interval, (int)TicketType.AllDays });

            int individualDays = DAO.Count<Ticket>(
                new string[] { "soldTime", "type" },
                new object[] { interval, new object[] { (int)TicketType.Day1, (int)TicketType.Day2, (int)TicketType.Day3 } });

            return allDays * 400 + individualDays* 200;
        }

        public bool SellTicket(int seller, TicketType ticketType)
        {
            var settings = DAO.Select<Setting>(
                new string[] { "name" }, new object[] { "MAX_CAPACITY" });

            int maxCapacity = settings[0].Value;

            int allDayTickets = DAO.Count<Ticket>(
                new string[] { "type" }, new object[] { (int)TicketType.AllDays });

            if (ticketType != TicketType.AllDays)
            {
                int currentDayTickets = DAO.Count<Ticket>(
                    new string[] { "type" }, new object[] { (int)ticketType });

                if (allDayTickets + currentDayTickets >= maxCapacity)
                {
                    return false;
                }
            }
            else
            {
                int day1Tickets = DAO.Count<Ticket>(
                    new string[] { "type" }, new object[] { (int)TicketType.Day1 });
                int day2Tickets = DAO.Count<Ticket>(
                    new string[] { "type" }, new object[] { (int)TicketType.Day2 });
                int day3Tickets = DAO.Count<Ticket>(
                    new string[] { "type" }, new object[] { (int)TicketType.Day3 });

                if (allDayTickets + day1Tickets >= maxCapacity ||
                    allDayTickets + day2Tickets >= maxCapacity ||
                    allDayTickets + day3Tickets >= maxCapacity)
                {
                    return false;
                }
            }

            DAO.Insert(new Ticket(seller, (int)ticketType, "now()"), true);
            PrintTicket(ticketType);

            return true;
        }

        private void PrintTicket(TicketType ticketType)
        {
            string receiptFileName = string.Format("ticket_{0}_{1}.txt",
                    DateTime.Now.ToString("yyMMdd"),
                    DateTime.Now.ToString("HHmmss"));

            var writer = new StreamWriter(receiptFileName);
            writer.WriteLine("************************");
            writer.WriteLine("*Electric Castle Ticket*");
            writer.WriteLine("************************");

            switch (ticketType)
            {
                case TicketType.AllDays:
                    writer.WriteLine("***Day1***Day2***Day3***");
                    break;
                case TicketType.Day1:
                    writer.WriteLine("***Day1*****************");
                    break;
                case TicketType.Day2:
                    writer.WriteLine("**********Day2**********");
                    break;
                case TicketType.Day3:
                    writer.WriteLine("*****************Day3***");
                    break;
            }

            writer.WriteLine(string.Format("*********{0} RON********",
                ticketType == TicketType.AllDays ? 400 : 200));

            writer.WriteLine(string.Format("*{0}**{1}**",
                DateTime.Now.ToString("dd MMM yyyy"),
                DateTime.Now.ToString("HH:mm:ss")));

            writer.WriteLine("************************");
            writer.Close();

            Process.Start("notepad.exe", receiptFileName);
        }
    }
}
