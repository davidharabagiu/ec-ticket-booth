﻿using ECTicketBooth.DataAccess;

namespace ECTicketBooth.Services
{
    /// <summary>
    /// Login session management service
    /// </summary>
    class LoginService
    {
        private bool loggedIn;
        private bool isAdmin;
        private string userName;
        private int userId;

        public bool LoggedIn
        {
            get { return loggedIn; }
        }

        public bool IsAdmin
        {
            get { return loggedIn && isAdmin; }
        }

        public string UserName
        {
            get { return loggedIn ? userName : ""; }
        }

        public int UserId
        {
            get { return loggedIn ? userId : -1; }
        }

        public LoginService()
        {
            loggedIn = false;
            isAdmin = false;
            userId = -1;
        }

        /// <summary>
        /// Start a login session. If a session is already ongoing, it will fail
        /// </summary>
        /// <param name="username">Name of the user account</param>
        /// <param name="password">Password of the user account</param>
        /// <returns>true if the login was successful, false otherwise</returns>
        public bool Login(string username, string password)
        {
            if (loggedIn)
            {
                return false;
            }

            var cashierData = DAO.Select<Cashier>(
                new string[] { "name ", "password" },
                new object[] { username, password });

            if (cashierData.Count == 0)
            {
                return false;
            }

            loggedIn = true;
            userName = cashierData[0].Name;
            userId = cashierData[0].Id;

            var adminData = DAO.Select<Administrator>(
                new string[] { "id" },
                new object[] { cashierData[0].Id });

            if (adminData.Count > 0)
            {
                isAdmin = true;
            }

            return true;
        }

        /// <summary>
        /// Closes the current login session
        /// </summary>
        public void Logout()
        {
            loggedIn = false;
            isAdmin = false;
            userId = -1;
        }
    }
}
