﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECTicketBooth.DataAccess;

namespace ECTicketBooth.Services
{
    enum UserOperationResponse
    {
        Success,
        UsernameEmpty,
        PasswordEmpty,
        UsernameTaken,
        NonExistentUser,
        NotPrivileged,
        HasSoldTickets
    }

    class UserAccountService
    {
        private TicketService ticketService;

        public List<string> UserList
        {
            get { return DAO.Select<Cashier>().Select(u => u.Name).ToList(); }
        }

        public UserAccountService()
        {
            ticketService = new TicketService();
        }

        public UserOperationResponse CreateAccount(string username, string password)
        {
            if (username == null || username == "")
            {
                return UserOperationResponse.UsernameEmpty;
            }
            else if (password == null || password == "")
            {
                return UserOperationResponse.PasswordEmpty;
            }
            else if (DAO.Count<Cashier>(new string[] { "name" }, new object[] { username }) > 0)
            {
                return UserOperationResponse.UsernameTaken;
            }

            DAO.Insert(new Cashier(username, password), true);
            return UserOperationResponse.Success;
        }

        public UserOperationResponse DeleteAccount(string username)
        {
            if (username == null || username == "")
            {
                return UserOperationResponse.NonExistentUser;
            }
            else if (DAO.Count<Cashier>(new string[] { "name" }, new object[] { username }) == 0)
            {
                return UserOperationResponse.NonExistentUser;
            }

            int userId = GetUserId(username);

            if (IsAdmin(userId))
            {
                return UserOperationResponse.NotPrivileged;
            }

            if (ticketService.GetProfitByCashier(userId) > 0)
            {
                return UserOperationResponse.HasSoldTickets;
            }

            DAO.Delete<Cashier>(new string[] { "id" }, new object[] { userId });
            return UserOperationResponse.Success;
        }

        public UserOperationResponse ChangePassword(string username, string newPassword)
        {
            if (username == null || username == "")
            {
                return UserOperationResponse.NonExistentUser;
            }
            else if (DAO.Count<Cashier>(new string[] { "name" }, new object[] { username }) == 0)
            {
                return UserOperationResponse.NonExistentUser;
            }

            int userId = GetUserId(username);

            if (IsAdmin(userId))
            {
                return UserOperationResponse.NotPrivileged;
            }

            DAO.Update(new Cashier(userId, username, newPassword));
            return UserOperationResponse.Success;
        }

        public int GetUserId(string username)
        {
            return DAO.Select<Cashier>(new string[] { "name" }, new object[] { username })[0].Id;
        }

        public bool IsAdmin(int userId)
        {
            return DAO.Count<Administrator>(new string[] { "id" }, new object[] { userId }) > 0;
        }
    }
}
