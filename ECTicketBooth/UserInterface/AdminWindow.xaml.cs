﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ECTicketBooth.Services;

namespace ECTicketBooth.UserInterface
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        private TicketService ticketService;
        private AppSettingsService appSettingsService;
        private UserAccountService userAccountService;

        public AdminWindow()
        {
            InitializeComponent();
            ticketService = new TicketService();
            appSettingsService = new AppSettingsService();
            userAccountService = new UserAccountService();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            int todayProfit = ticketService.GetProfitByDay(DateTime.Today);
            int oneDayAgoProfit = ticketService.GetProfitByDay(DateTime.Today.AddDays(-1));
            int twoDaysAgoProfit = ticketService.GetProfitByDay(DateTime.Today.AddDays(-2));
            int totalProfit = todayProfit + oneDayAgoProfit + twoDaysAgoProfit;

            TextToday.Text = todayProfit.ToString() + " RON";
            Text1DayAgo.Text = oneDayAgoProfit.ToString() + " RON";
            Text2DaysAgo.Text = twoDaysAgoProfit.ToString() + " RON";
            TextTotal.Text = totalProfit.ToString() + " RON";

            TextMaxCapacity.Text = appSettingsService.GetValue("MAX_CAPACITY").ToString();
            UpdateCashierList();
        }

        private void UpdateCashierList()
        {
            CashierList.Items.Clear();
            userAccountService.UserList.ForEach(a => CashierList.Items.Add(a));
        }

        private void ChangeMaxCapacity_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int newValue = Convert.ToInt32(TextMaxCapacity.Text);
                if (newValue < ticketService.MaxSold)
                {
                    MessageBox.Show("The new value of MAX_CAPACITY is less than the number of tickets already sold.");
                    TextMaxCapacity.Text = appSettingsService.GetValue("MAX_CAPACITY").ToString();
                    return;
                }

                appSettingsService.SetValue("MAX_CAPACITY", newValue);
            }
            catch (FormatException ex)
            {
                MessageBox.Show("Invalid value for MAX_CAPACITY.\n\n" + ex.Message);
            }
        }

        private void CreateUserButton_Click(object sender, RoutedEventArgs e)
        {
            var resposnse = userAccountService.CreateAccount(UsernameText.Text, PasswordText.Password);
            if (resposnse == UserOperationResponse.UsernameEmpty)
                MessageBox.Show("The username can't be empty.");
            else if (resposnse == UserOperationResponse.PasswordEmpty)
                MessageBox.Show("The password can't be empty.");
            else if (resposnse == UserOperationResponse.UsernameTaken)
                MessageBox.Show("This username was already taken.");
            else if (resposnse == UserOperationResponse.Success)
            {
                UpdateCashierList();
                UsernameText.Clear();
                PasswordText.Clear();
            }
        }

        private void CashierList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (CashierList.SelectedItem == null)
            {
                return;
            }

            UserControl userControlWindow = new UserControl();
            userControlWindow.SetUser(CashierList.SelectedItem.ToString());
            userControlWindow.ShowDialog();
            UpdateCashierList();
        }
    }
}
