﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ECTicketBooth.Services;

namespace ECTicketBooth.UserInterface
{
    /// <summary>
    /// Interaction logic for UserControl.xaml
    /// </summary>
    public partial class UserControl : Window
    {
        private string username;
        private int userId;
        private TicketService ticketService;
        private UserAccountService userAccountService;

        public UserControl()
        {
            InitializeComponent();
            ticketService = new TicketService();
            userAccountService = new UserAccountService();
        }

        public void SetUser(string username)
        {
            Title = "Ticket Booth - Edit " + username;
            this.username = username;
            userId = userAccountService.GetUserId(username);
            int ticketsSold = ticketService.GetTicketsSoldCount(userId);
            bool isAdmin = userAccountService.IsAdmin(userId);
            SoldText.Text = ticketsSold.ToString();
            if (ticketsSold > 0 || isAdmin)
            {
                DeleteButton.IsEnabled = false;
            }
            if (isAdmin)
            {
                ChangePassButton.IsEnabled = false;
            }
        }

        private void ChangePassButton_Click(object sender, RoutedEventArgs e)
        {
            var response = userAccountService.ChangePassword(username, NewPassText.Password);
            if (response == UserOperationResponse.NonExistentUser)
            {
                MessageBox.Show("This user does not exist.");
            }
            else if (response == UserOperationResponse.NotPrivileged)
            {
                MessageBox.Show("Illegal operation.");
            }
            else if (response == UserOperationResponse.PasswordEmpty)
            {
                MessageBox.Show("Password can't be empty.");
            }
            else if (response == UserOperationResponse.Success)
            {
                MessageBox.Show("Passowrd changed successfully.");
                NewPassText.Clear();
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var response = userAccountService.DeleteAccount(username);
            if (response == UserOperationResponse.NonExistentUser)
            {
                MessageBox.Show("This user does not exist.");
            }
            else if (response == UserOperationResponse.NotPrivileged ||
                response == UserOperationResponse.HasSoldTickets)
            {
                MessageBox.Show("Illegal operation.");
            }
            else if (response == UserOperationResponse.Success)
            {
                Close();
            }
        }
    }
}
