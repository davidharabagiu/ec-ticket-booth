﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ECTicketBooth.Services;

namespace ECTicketBooth.UserInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private LoginService loginService;
        private TicketService ticketService;
        private AdminWindow adminWindow;

        public MainWindow()
        {
            InitializeComponent();
            loginService = new LoginService();
            ticketService = new TicketService();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            if (loginService.Login(UsernameTextBox.Text, PasswordTextBox.Password))
            {
                if (CheckRemember.IsChecked.HasValue && CheckRemember.IsChecked.Value)
                {
                    ECTicketBooth.Properties.Settings.Default.Username = UsernameTextBox.Text;
                    ECTicketBooth.Properties.Settings.Default.Password = PasswordTextBox.Password;
                    ECTicketBooth.Properties.Settings.Default.LoginRemembered = true;
                }
                else
                {
                    ECTicketBooth.Properties.Settings.Default.Username = "";
                    ECTicketBooth.Properties.Settings.Default.Password = "";
                    ECTicketBooth.Properties.Settings.Default.LoginRemembered = false;
                }

                ECTicketBooth.Properties.Settings.Default.Save();

                if (loginService.IsAdmin)
                {
                    AdminButton.Visibility = Visibility.Visible;
                }
                else
                {
                    AdminButton.Visibility = Visibility.Hidden;
                }

                Title = "Ticket Booth - " + loginService.UserName;
                UpdateTicketHistory();
                LoginPanel.Visibility = Visibility.Hidden;
                CashierPanel.Visibility = Visibility.Visible;
            }
            else
            {
                MessageBox.Show("Login failed.");
                UsernameTextBox.Text = "";
                PasswordTextBox.Password = "";
            }
        }

        private void LogoImage_MouseEnter(object sender, MouseEventArgs e)
        {
            LogoImage.Source = new BitmapImage(new Uri("../Images/eclogo2.jpg", UriKind.Relative));
        }

        private void LogoImage_MouseLeave(object sender, MouseEventArgs e)
        {
            LogoImage.Source = new BitmapImage(new Uri("../Images/eclogo.jpg", UriKind.Relative));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (ECTicketBooth.Properties.Settings.Default.LoginRemembered)
            {
                UsernameTextBox.Text = ECTicketBooth.Properties.Settings.Default.Username;
                PasswordTextBox.Password = ECTicketBooth.Properties.Settings.Default.Password;
                CheckRemember.IsChecked = true;
            }
        }

        private void UpdateTicketHistory()
        {
            TicketList.Items.Clear();
            ticketService.GetTicketHistory(loginService.UserId).ForEach(t => TicketList.Items.Add(t));
            TextTotal.Text = "Total: " + ticketService.GetProfitByCashier(loginService.UserId).ToString() + " RON";
        }

        private void SellButton_Click(object sender, RoutedEventArgs e)
        {
            TicketType ticketType;
            if (RadioAllDays.IsChecked.HasValue && RadioAllDays.IsChecked.Value)
                ticketType = TicketType.AllDays;
            else if (RadioDay1.IsChecked.HasValue && RadioDay1.IsChecked.Value)
                ticketType = TicketType.Day1;
            else if (RadioDay2.IsChecked.HasValue && RadioDay2.IsChecked.Value)
                ticketType = TicketType.Day2;
            else if (RadioDay3.IsChecked.HasValue && RadioDay3.IsChecked.Value)
                ticketType = TicketType.Day3;
            else
                return;

            if (ticketService.SellTicket(loginService.UserId, ticketType))
            {                
                UpdateTicketHistory();
            }
            else
            {
                MessageBox.Show("Sold out!");
            }
        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            if (adminWindow != null)
            {
                adminWindow.Close();
                adminWindow = null;
            }

            loginService.Logout();

            if (!CheckRemember.IsChecked.HasValue || !CheckRemember.IsChecked.Value)
            {
                UsernameTextBox.Text = "";
                PasswordTextBox.Password = "";
            }

            CashierPanel.Visibility = Visibility.Hidden;
            LoginPanel.Visibility = Visibility.Visible;

            Title = "Ticket Booth";
        }

        private void AdminButton_Click(object sender, RoutedEventArgs e)
        {
            adminWindow = new AdminWindow();
            adminWindow.Show();
        }
    }
}
