﻿namespace ECTicketBooth.DataAccess
{
    class Cashier
    {   
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }

        public Cashier(int id, string name, string password)
        {
            Id = id;
            Name = name;
            Password = password;
        }

        public Cashier(string name, string password)
            : this(0, name, password)
        {
            
        }

        public Cashier(int id)
            : this(id, "", "")
        {

        }
    }
}
