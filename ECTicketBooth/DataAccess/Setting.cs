﻿namespace ECTicketBooth.DataAccess
{
    class Setting
    {
        public string Name { get; set; }
        public int Value { get; set; }

        public Setting(string name, int value)
        {
            Name = name;
            Value = value;
        }

    }
}
