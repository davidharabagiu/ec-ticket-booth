﻿using System;

namespace ECTicketBooth.DataAccess
{
    class DataInterval<T>
    {
        private Tuple<T, T> tuple;
        private bool lowerOpen;
        private bool upperOpen;

        public T Item1 { get { return tuple.Item1; } }
        public T Item2 { get { return tuple.Item2; } }
        public bool LowerOpen { get { return lowerOpen; } }
        public bool UpperOpen { get { return upperOpen; } }

        public DataInterval(T i1, T i2, bool lowerOpen, bool upperOpen)
        {
            tuple = new Tuple<T, T>(i1, i2);
            this.lowerOpen = lowerOpen;
            this.upperOpen = upperOpen;
        }

        public DataInterval(T i1, T i2)
            : this(i1, i2, false, false)
        {

        }

    }
}
