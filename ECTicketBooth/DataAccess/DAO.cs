﻿using System;
using System.Collections.Generic;

namespace ECTicketBooth.DataAccess
{
    /// <summary>
    /// Static class containing methods corresponding to CRUD operations on the database
    /// </summary>
    static class DAO
    {
        /// <summary>
        /// Insert an object into the database
        /// </summary>
        /// <param name="obj">The object to be inserted</param>
        /// <param name="ignoreFirst">Set this parameter to true to ignore the first field, 
        /// if this field has the auto_increment property in the database</param>
        public static void Insert(object obj, bool ignoreFirst)
        {
            string cmdString = string.Format(
                "insert into {0} (",
                obj.GetType().Name);

            var fields = obj.GetType().GetProperties();

            for (int i = ignoreFirst ? 1 : 0; i < fields.Length; ++i)
            {
                cmdString += fields[i].Name;

                if (i != fields.Length - 1)
                {
                    cmdString += ",";
                }
            }

            cmdString += ") values (";

            for (int i = ignoreFirst ? 1 : 0; i < fields.Length; ++i)
            {
                bool inQuotes = false;
                if (fields[i].PropertyType.Name.ToLower() == "string" &&
                    fields[i].GetValue(obj).ToString() != "now()")
                {
                    inQuotes = true;
                }

                //cmdString += fields[i].FieldType.Name + ":";
                cmdString += inQuotes ? "\'" : "";
                cmdString += fields[i].GetValue(obj).ToString();
                cmdString += inQuotes ? "\'" : "";

                if (i != fields.Length - 1)
                {
                    cmdString += ",";
                }
            }

            cmdString += ")";
            DBConnection.Instance.Execute(cmdString);
        }

        /// <summary>
        /// Remove an object from the database
        /// </summary>
        /// <param name="fieldNames">The names of the fields for the objects to be filtered by</param>
        /// <param name="fieldValues">The values of the fields for the objects to be filtered by</param>
        public static void Delete<T>(string[] fieldNames, object[] fieldValues)
        {
            string cmdString = string.Format("delete from {0}", typeof(T).Name);

            if (fieldNames != null && fieldValues != null && fieldNames.Length > 0)
            {
                cmdString += " where ";

                for (int i = 0; i < fieldNames.Length; ++i)
                {
                    cmdString += string.Format("{0} = {2}{1}{2}",
                        fieldNames[i],
                        fieldValues[i].ToString(),
                        fieldValues[i].GetType().Name.ToLower() == "string" ? "\'" : "");

                    if (i != fieldNames.Length - 1)
                    {
                        cmdString += " and ";
                    }
                }
            }

            DBConnection.Instance.Execute(cmdString);
        }

        /// <summary>
        /// Updates an object in the database with new values
        /// </summary>
        /// <param name="obj">The object to be updated which has the new values</param>
        public static void Update(object obj)
        {
            string cmdString = string.Format(
                "update {0} set ",
                obj.GetType().Name);

            var fields = obj.GetType().GetProperties();

            bool inQuotes = false;
            for (int i = 1; i < fields.Length; ++i)
            {
                cmdString += fields[i].Name + " = ";
                
                if (fields[i].PropertyType.Name.ToLower() == "string")
                {
                    inQuotes = true;
                }

                cmdString += inQuotes ? "\'" : "";
                cmdString += fields[i].GetValue(obj).ToString();
                cmdString += inQuotes ? "\'" : "";

                if (i != fields.Length - 1)
                {
                    cmdString += ", ";
                }
            }

            inQuotes = fields[0].PropertyType.Name.ToLower() == "string";
            cmdString += string.Format(" where {0} = {2}{1}{2}",
                fields[0].Name, fields[0].GetValue(obj), inQuotes ? "\'" : "");

            DBConnection.Instance.Execute(cmdString);
        }

        /// <summary>
        /// Returns a list of all objects of a given type
        /// </summary>
        /// <typeparam name="T">The type of the objects to be returned</typeparam>
        /// <returns>The list of objects</returns>
        public static List<T> Select<T>()
        {
            return Select<T>(null, null);
        }

        /// <summary>
        /// Returns a list of objects of a given type that meet a set of criterias
        /// </summary>
        /// <typeparam name="T">The type of the objects to be returned</typeparam>
        /// <param name="fieldNames">The names of the fields for the objects to be filtered by</param>
        /// <param name="fieldValues">The values of the fields for the objects to be fitered by</param>
        /// <returns>The list of objects</returns>
        public static List<T> Select<T>(string[] fieldNames, object[] fieldValues)
        {
            if (fieldNames != null && fieldValues != null && fieldNames.Length != fieldValues.Length)
            {
                return null;
            }

            string cmdString = string.Format(
                "select * from {0}",
                typeof(T).Name);

            if (fieldNames != null && fieldValues != null && fieldNames.Length > 0)
            {
                cmdString += " where ";

                for (int i = 0; i < fieldNames.Length; ++i)
                {
                    if (fieldValues[i].GetType().IsGenericType &&
                        fieldValues[i].GetType().GetGenericTypeDefinition() == typeof(DataInterval<>))
                    {
                        var val1 = fieldValues[i].GetType().GetProperty("Item1").GetValue(fieldValues[i]);
                        var val2 = fieldValues[i].GetType().GetProperty("Item2").GetValue(fieldValues[i]);
                        bool lowerOpen = Convert.ToBoolean(fieldValues[i].GetType().GetProperty("LowerOpen").GetValue(fieldValues[i]));
                        bool upperOpen = Convert.ToBoolean(fieldValues[i].GetType().GetProperty("UpperOpen").GetValue(fieldValues[i]));

                        cmdString += string.Format("{0} {1} {5}{2}{5} and {0} {3} {5}{4}{5}",
                            fieldNames[i],
                            lowerOpen ? ">=" : ">",
                            val1.ToString(),
                            upperOpen ? "<=" : "<",
                            val2.ToString(),
                            val1.GetType().Name.ToLower() == "string" ? "\'" : "");
                    }
                    else if (fieldValues[i].GetType().IsArray)
                    {
                        cmdString += "(";

                        var arrValues = (Array)fieldValues[i];
                        for (int j = 0; j < arrValues.Length; ++j)
                        {
                            cmdString += string.Format("{0} = {2}{1}{2}",
                                fieldNames[i],
                                arrValues.GetValue(j),
                                arrValues.GetValue(j).GetType().Name.ToLower() == "string" ? "\'" : "");

                            if (j != arrValues.Length - 1)
                            {
                                cmdString += " or ";
                            }
                            else
                            {
                                cmdString += ")";
                            }
                        }
                    }
                    else
                    {
                        cmdString += string.Format("{0} = {2}{1}{2}",
                        fieldNames[i],
                        fieldValues[i].ToString(),
                        fieldValues[i].GetType().Name.ToLower() == "string" ? "\'" : "");
                    }

                    if (i != fieldNames.Length - 1)
                    {
                        cmdString += " and ";
                    }
                }
            }

            var objects = new List<T>();
            var dbData = DBConnection.Instance.Query(cmdString);
            var fields = typeof(T).GetProperties();
            var types = new Type[fields.Length];

            for (int i = 0; i < types.Length; ++i)
            {
                types[i] = fields[i].PropertyType;
            }

            var ctor = typeof(T).GetConstructors()[0];

            for (int i = 0; i < dbData[0].Count; ++i)
            {
                var fieldData = new object[dbData.Length];
                for (int j = 0; j < dbData.Length; ++j)
                {
                    fieldData[j] = dbData[j][i];
                }
                objects.Add((T)ctor.Invoke(fieldData));
            }

            return objects;
        }

        public static int Count<T>(string[] fieldNames, object[] fieldValues)
        {
            if (fieldNames != null && fieldValues != null && fieldNames.Length != fieldValues.Length)
            {
                return 0;
            }

            string cmdString = string.Format(
                "select count(*) from {0}",
                typeof(T).Name);

            if (fieldNames != null && fieldValues != null && fieldNames.Length > 0)
            {
                cmdString += " where ";

                for (int i = 0; i < fieldNames.Length; ++i)
                {
                    if (fieldValues[i].GetType().IsGenericType &&
                        fieldValues[i].GetType().GetGenericTypeDefinition() == typeof(DataInterval<>))
                    {
                        var val1 = fieldValues[i].GetType().GetProperty("Item1").GetValue(fieldValues[i]);
                        var val2 = fieldValues[i].GetType().GetProperty("Item2").GetValue(fieldValues[i]);
                        bool lowerOpen = Convert.ToBoolean(fieldValues[i].GetType().GetProperty("LowerOpen").GetValue(fieldValues[i]));
                        bool upperOpen = Convert.ToBoolean(fieldValues[i].GetType().GetProperty("UpperOpen").GetValue(fieldValues[i]));

                        cmdString += string.Format("{0} {1} {5}{2}{5} and {0} {3} {5}{4}{5}",
                            fieldNames[i],
                            lowerOpen ? ">=" : ">",
                            val1.ToString(),
                            upperOpen ? "<=" : "<",
                            val2.ToString(),
                            val1.GetType().Name.ToLower() == "string" ? "\'" : "");
                    }
                    else if (fieldValues[i].GetType().IsArray)
                    {
                        cmdString += "(";

                        var arrValues = (Array)fieldValues[i];
                        for (int j = 0; j < arrValues.Length; ++j)
                        {
                            cmdString += string.Format("{0} = {2}{1}{2}",
                                fieldNames[i],
                                arrValues.GetValue(j),
                                arrValues.GetValue(j).GetType().Name.ToLower() == "string" ? "\'" : "");

                            if (j != arrValues.Length - 1)
                            {
                                cmdString += " or ";
                            }
                            else
                            {
                                cmdString += ")";
                            }
                        }
                    }
                    else
                    {
                        cmdString += string.Format("{0} = {2}{1}{2}",
                        fieldNames[i],
                        fieldValues[i].ToString(),
                        fieldValues[i].GetType().Name.ToLower() == "string" ? "\'" : "");
                    }

                    if (i != fieldNames.Length - 1)
                    {
                        cmdString += " and ";
                    }
                }
            }

            var dbData = DBConnection.Instance.Query(cmdString);

            return Convert.ToInt32(dbData[0][0]);
        }
    }
}
