﻿using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace ECTicketBooth.DataAccess
{
    /// <summary>
    /// Database Connection Logic
    /// </summary>
    class DBConnection
    {
        private static DBConnection instance = null;

        private MySqlConnection connection;
        private string connectionString = ECTicketBooth.Properties.Resources.DBConnectionString;

        public static DBConnection Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DBConnection();
                }

                return instance;
            }
        }

        private DBConnection()
        {

        }

        /// <summary>
        /// Open a connection to the database
        /// </summary>
        private void Connect()
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
        }

        /// <summary>
        /// Close the connection to the database
        /// </summary>
        private void Disconnect()
        {
            connection.Close();
        }

        /// <summary>
        /// Execute a non-query command
        /// </summary>
        /// <param name="cmdString">Command text</param>
        public void Execute(string cmdString)
        {
            Connect();
            var cmd = new MySqlCommand(cmdString, connection);
            cmd.ExecuteNonQuery();
            Disconnect();
        }

        /// <summary>
        /// Execute a query command
        /// </summary>
        /// <param name="cmdString">Command text</param>
        /// <returns>An array of lists containing the data. Each list contains the values of a certain field.</returns>
        public List<object>[] Query(string cmdString)
        {
            Connect();

            var cmd = new MySqlCommand(cmdString, connection);
            var reader = cmd.ExecuteReader();

            List<object>[] result = new List<object>[reader.FieldCount];
            for (int i = 0; i < result.Length; ++i)
            {
                result[i] = new List<object>();
            }

            while (reader.Read())
            {
                for (int i = 0; i < result.Length; ++i)
                {
                    result[i].Add(reader[i]);
                }
            }

            reader.Close();
            Disconnect();

            return result;
        }
    }
}
