﻿namespace ECTicketBooth.DataAccess
{
    class Ticket
    {
        public int Id { get; set; }
        public int Seller { get; set; }
        public int Type { get; set; }
        public string SoldTime { get; set; }

        public Ticket(int id, int seller, int type, System.DateTime soldTime)
            : this(id, seller, type, soldTime.ToString())
        {

        }

        public Ticket(int id, int seller, int type, string soldTime)
        {
            Id = id;
            Seller = seller;
            Type = type;
            SoldTime = soldTime;
        }

        public Ticket(int seller, int type, string soldTime)
            : this(0, seller, type, soldTime)
        {

        }

        public Ticket(int id)
            : this(id, 0, 0, "")
        {

        }
    }
}
