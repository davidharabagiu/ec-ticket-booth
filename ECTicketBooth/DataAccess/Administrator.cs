﻿namespace ECTicketBooth.DataAccess
{
    class Administrator
    {
        public int Id { get; set; }
        
        public Administrator(int id)
        {
            Id = id;
        }
    }
}
