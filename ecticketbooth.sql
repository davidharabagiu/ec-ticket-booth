create database if not exists ecticketbooth;
use ecticketbooth;

create table if not exists cashier (
id int not null auto_increment,
name varchar(30) not null,
password varchar(30) not null,
primary key (id)
);

create table if not exists administrator (
id int not null,
primary key (id),
foreign key (id) references cashier (id)
);

create table if not exists ticket (
id int not null auto_increment,
seller int not null,
type int not null,
soldTime datetime not null,
primary key (id),
foreign key (seller) references cashier (id)
);

create table if not exists setting (
name varchar (30) not null,
value int not null,
primary key (name),
);